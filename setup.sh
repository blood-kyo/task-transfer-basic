./network.sh down
./network.sh up createChannel -c mychannel -ca -s couchdb & PIDCC=$!
gnome-terminal --tab -- sh -c "cd /home/yellow/go/src/github.com/hyperledger/fabric/scripts/fabric-samples/task-transfer-basic/fabric-ipfs-app;code .;ipfs daemon;bash" &
gnome-terminal --tab -- sh -c "cd /home/yellow/go/src/github.com/hyperledger/fabric/scripts/fabric-samples/task-transfer-basic/fabric-ipfs-app;cd client;npm run serve;bash" &
gnome-terminal --tab -- sh -c "cd /home/yellow/go/src/github.com/hyperledger/fabric/scripts/fabric-samples/task-transfer-basic/fabric-ipfs-app;cd server;npm run devStart;bash" &
wait $PIDCC
gnome-terminal --tab -- sh -c "./monitordocker.sh net_test"
./network.sh deployCC -ccn basic -ccp ../task-transfer-basic/chaincode-go/ -ccl go
