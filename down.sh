docker network disconnect -f net_test logspout
./network.sh down

# delete the wallet folder in fabric-ipfs-app/server/wallet, get ready for the next set up.
rm -r ../task-transfer-basic/fabric-ipfs-app/server/wallet/*
rm -r ../task-transfer-basic/fabric-ipfs-app/server/files/*